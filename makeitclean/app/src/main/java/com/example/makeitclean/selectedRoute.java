 package com.example.makeitclean;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

 public class selectedRoute extends AppCompatActivity {
     private DatabaseReference mDatabase;
     TextView routeName;
     TextView Postcode;
     RatingBar congestion_rating2;
     RatingBar polution_bar2;
     @Override
     protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_selected_route);
         Intent secondIntent = getIntent();
         routeName = (TextView) findViewById(R.id.routeName_2);
         Postcode = (TextView) findViewById(R.id.Postcode_2);
         congestion_rating2 = (RatingBar) findViewById(R.id.congestion_rating2);
         polution_bar2 = (RatingBar) findViewById(R.id.polution_bar2);
         mDatabase = FirebaseDatabase.getInstance().getReference().child("route");
         mDatabase.child(secondIntent.getStringExtra("COURSE_SELECTED")).child("routeName").get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
             @Override
             public void onComplete(@NonNull Task<DataSnapshot> task) {
                 if (!task.isSuccessful()) {
                     Log.e("firebase", "Error getting data", task.getException());
                 }
                 else {
                     Log.d("firebase", String.valueOf(task.getResult().getValue()));
                     String stringroute = String.valueOf(task.getResult().getValue());
                     routeName.setText(stringroute);
                 }
             }
         });
         mDatabase.child(secondIntent.getStringExtra("COURSE_SELECTED")).child("postcode").get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
             @Override
             public void onComplete(@NonNull Task<DataSnapshot> task) {
                 if (!task.isSuccessful()) {
                     Log.e("firebase", "Error getting data", task.getException());
                 }
                 else {
                     Log.d("firebase", String.valueOf(task.getResult().getValue()));
                     String stringpostcode = String.valueOf(task.getResult().getValue());
                     Postcode.setText(stringpostcode);

                 }
             }
         });
        mDatabase.child(secondIntent.getStringExtra("COURSE_SELECTED")).child("con_bar").get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DataSnapshot> task) {
                if (!task.isSuccessful()) {
                    Log.e("firebase", "Error getting data", task.getException());
                }
                else {
                    Log.d("firebase", String.valueOf(task.getResult().getValue()));
                    String stringrating = String.valueOf(task.getResult().getValue());
                    float rating = Float.parseFloat(stringrating);
                    congestion_rating2.setRating(rating);

                }
            }
        });
         mDatabase.child(secondIntent.getStringExtra("COURSE_SELECTED")).child("pol_bar").get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
             @Override
             public void onComplete(@NonNull Task<DataSnapshot> task) {
                 if (!task.isSuccessful()) {
                     Log.e("firebase", "Error getting data", task.getException());
                 }
                 else {
                     Log.d("firebase", String.valueOf(task.getResult().getValue()));
                     String stringrating = String.valueOf(task.getResult().getValue());
                     float rating = Float.parseFloat(stringrating);
                     polution_bar2.setRating(rating);

                 }
             }
         });
     }

     public void setIsIndicator (boolean isIndicator){

     }
     @Override
     public boolean onCreateOptionsMenu(Menu menu) {
         getMenuInflater().inflate(R.menu.options_menu, menu);
         return true;
     }
     @Override
     public boolean onOptionsItemSelected(MenuItem item) {
         switch (item.getItemId()) {
             case R.id.home:
                 startActivity(new Intent(selectedRoute.this, MainMenu.class));
                 return true;
             case R.id.Settings:
                 // do your code
                 startActivity(new Intent(selectedRoute.this, Settings.class));
                 return true;
             case R.id.SignOut:
                 // do your code
                 FirebaseAuth.getInstance().signOut();
                 startActivity(new Intent(selectedRoute.this, MainActivity.class));
                 Toast.makeText(selectedRoute.this, "Logout Successfully!", Toast.LENGTH_SHORT).show();
                 return true;
             case R.id.addRoute:
                 // do your code
                 startActivity(new Intent(selectedRoute.this, addRoutes.class));
                 return true;

             default:
                 return super.onOptionsItemSelected(item);
         }
     }
}