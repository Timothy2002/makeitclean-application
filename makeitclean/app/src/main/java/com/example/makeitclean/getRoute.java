package com.example.makeitclean;

public class getRoute {
    public String routeName;
    public String postcode;
    public Float con_bar;
    public Float pol_bar;

    public getRoute(String routeName,String postcode, Float con_bar, Float pol_bar){
        this.routeName = routeName;
        this.postcode = postcode;
        this.con_bar = con_bar;
        this.pol_bar = pol_bar;
    }
}
