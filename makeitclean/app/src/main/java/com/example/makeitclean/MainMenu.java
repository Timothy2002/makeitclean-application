package com.example.makeitclean;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MainMenu extends AppCompatActivity {
    private DatabaseReference mDatabase;
    private ListView listView;
    private Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        intent = new Intent(this, selectedRoute.class);
        listView = findViewById(R.id.listRoute);

        ArrayList<String> list = new ArrayList<>();
        ArrayAdapter adapter = new ArrayAdapter<String>(this , R.layout.list_item, list);
        listView.setAdapter(adapter);
        mDatabase = FirebaseDatabase.getInstance().getReference().child("route");
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                list.clear();
                for (DataSnapshot snapshot1 : snapshot.getChildren()){
                    list.add(snapshot1.child("routeName").getValue().toString());
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        listView.setOnItemClickListener(listClick);

    }
    private AdapterView.OnItemClickListener listClick = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            Integer value = position+1;
            String Value = value.toString();
            intent.putExtra("COURSE_SELECTED", Value);
            startActivity(intent);
        }
    };
    public void onselect(View view){
        startActivity(new Intent(MainMenu.this, selectedRoute.class));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                startActivity(new Intent(MainMenu.this, MainMenu.class));
                return true;
            case R.id.Settings:
                // do your code
                startActivity(new Intent(MainMenu.this, Settings.class));
                return true;
            case R.id.SignOut:
                // do your code
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(MainMenu.this, MainActivity.class));
                Toast.makeText(MainMenu.this, "Logout Successfully!", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.addRoute:
                // do your code
                startActivity(new Intent(MainMenu.this, addRoutes.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void route1(View view){
        startActivity(new Intent(MainMenu.this, selectedRoute.class));
    }
}