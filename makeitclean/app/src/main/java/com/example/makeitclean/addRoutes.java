package com.example.makeitclean;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class addRoutes extends AppCompatActivity {
    long maxid = 0;
    private DatabaseReference mDatabase;
    private EditText RouteName;
    private EditText Postcode;
    private RatingBar Polution_bar;
    private RatingBar Congestion_rating;
    private Button btnsave;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_routes);
        RouteName = (EditText) findViewById(R.id.RouteName);
        Postcode = (EditText) findViewById(R.id.Postcode);
        Polution_bar = (RatingBar) findViewById(R.id.Polution_bar);
        Congestion_rating = (RatingBar) findViewById(R.id.Congestion_rating);
        btnsave = (Button)findViewById(R.id.routebutton);
        mDatabase = FirebaseDatabase.getInstance().getReference().child("route");



        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                    maxid = (snapshot.getChildrenCount());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    public void onNewRoute(View view){
        String routeName = RouteName.getText().toString().trim();
        String postcode = Postcode.getText().toString().trim();
        Float con_bar = Congestion_rating.getRating();
        Float pol_bar = Polution_bar.getRating();
        newroute(routeName, postcode, con_bar, pol_bar);
    }
    private void newroute(String routeName,String postcode, Float con_bar, Float pol_bar){
        addRoute addnewroute = new addRoute(routeName, postcode, con_bar, pol_bar);
        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDatabase.child(String.valueOf(maxid+1)).setValue(addnewroute);
                Toast.makeText(addRoutes.this,"Route added", Toast.LENGTH_LONG).show();
            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                startActivity(new Intent(addRoutes.this, MainMenu.class));
                return true;
            case R.id.Settings:
                // do your code
                startActivity(new Intent(addRoutes.this, Settings.class));
                return true;
            case R.id.SignOut:
                // do your code
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(addRoutes.this, MainActivity.class));
                Toast.makeText(addRoutes.this, "Logout Successfully!", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.addRoute:
                // do your code
                startActivity(new Intent(addRoutes.this, addRoutes.class));
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}